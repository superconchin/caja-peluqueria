<html>
	<head>
		<link href="/plp/css/index.css" rel="stylesheet">
    	<link href="/plp/css/bootstrap.css" rel="stylesheet">
    	<link href="/plp/css/bootstrap-responsive.css" rel="stylesheet">
    	<link rel="shortcut icon" href="/imagenes/favicon.ico">
	</head>
	<body>
		<h2 align="center">Buscar Producto <br></h2>
		<form action="/plp/php/mod_pvp.php" method="POST">
			<strong>Nombre del Producto:&nbsp&nbsp&nbsp</strong> <select name="prod" id="prod">
											<option value="producto">Producto</option>
											<option value="corte_nina">Corte Nina</option>
											<option value="corte_ninas">Corte Ninas</option>
											<option value="corte">Corte</option>
											<option value="corte_s">Corte Senora</option>
											<option value="corte_hombre">Corte Hombre</option>
											<option value="corte_nino">Corte Nino</option>
											<option value="bebe">Bebe</option>
											<option value="permanente">Permanente</option>
											<option value="suplemento_color">Suplemento color</option>
											<option value="tinte_raiz">Tinte Raiz</option>
											<option value="tinte_pc">Tinte Pelo Corto</option>
											<option value="tinte_pm">Tinte Melena</option>
											<option value="tinte_pl">Tinte Pelo Largo</option>
											<option value="bano_pc">Bano Pelo Corto</option>
											<option value="bano_pm">Bano Melena</option>
											<option value="bano_pl">Bano Pelo Largo</option>
											<option value="mechas_raiz">Mechas Raiz</option>
											<option value="mechas_pc">Mechas Pelo Corto</option>
											<option value="mechas_pm">Mechas Melena</option>
											<option value="mechas_pl">Mechas Pelo Largo</option>
											<option value="mechas_td">Mechas Todo</option>
											<option value="recogido">Recogido</option>
											<option value="semirecogido">Semirecogido</option>
											<option value="difusor">Difusor</option>
											<option value="plancha">Plancha</option>
											<option value="flequillo"></option>
										  </select><br>
			<strong>Precio:&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<input type="text" name="pvp_new" id="pvp_new"></strong><br>
			<input type="submit"value="MODIFICAR" class="btn btn-warning">
		</form>

	</body>
</html>