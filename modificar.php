<html>
	<head>
		<link href="css/index.css" rel="stylesheet">
    	<link href="css/bootstrap.css" rel="stylesheet">
    	<link href="css/bootstrap-responsive.css" rel="stylesheet">
    	<link rel="shortcut icon" href="imagenes/favicon.ico">
    	<script type="text/javascript" src="js/jquery-1.11.2.js"></script>
<script type="text/javascript">
	function alerta(){	
		alert("El precio ha sido modificado correctamente");	
	}
</script>
	</head>
	<body>
<?php
	include("php/funciones.php");
?>
	<div class="row-fluid">
		<div class="span12"><a href="index.php"><img src="imagenes/plp.jpg"></a></div>
	</div>
	<div class="row-fluid">
		<div class="span1"></div>
		<div class="span5">
			<h3 align="center">Modificar Producto <br></h3>
			<form action="php/mod_pvp.php" method="POST" name="formu">
				 <div class="form-group">
				    <label for="exampleFormControlSelect1"><strong>Nombre del Producto</strong></label>
				    <select class="form-control" id="exampleFormControlSelect1" name="prod">
				    	<?php productos_select();?>
				    </select>
  				</div>
				  <div class="form-group">
				    <label for="exampleFormControlInput1"><strong>Precio nuevo</strong></label>
				    <input type="text" class="form-control" id="exampleFormControlInput1" name="pvp_new">
				  </div> 
				  <button type="submit" class="btn btn-success">Modificar</button>
				  <button type="button" class="btn btn-danger" id="delete">Borrar</button> 				
		</form>
		</div>
		<div class="span5">
			<h3 align="center">Insertar Producto <br></h3>
			<form action="php/insertar_prod.php" method="POST" name="formu">
				 <div class="form-group">
				    <label for="exampleFormControlSelect1"><strong>Nombre del Producto Nuevo</strong></label>
				    <input type="text" class="form-control" id="exampleFormControlInput1" name="prod_new">
  				</div>
				  <div class="form-group">
				    <label for="exampleFormControlInput1"><strong>Precio nuevo</strong></label>
				    <input type="text" class="form-control" id="exampleFormControlInput1" name="pvp">
				  </div>
				  <div class="form-group">
				    <label for="exampleFormControlInput1"><strong>Color Boton</strong></label>
				    <input list="listacoloresperfil" type="color" class="form-control" id="exampleFormControlInput1" name="color">
				  </div>				   
				  <button type="submit" class="btn btn-primary">Insertar</button> 				
		</form>	
			<datalist id="listacoloresperfil">

  <option value="#00ffff">

  <option value="#ff00ff">

  <option value="#ffff00">

  <option value="#ffaa00">

</datalist>
		</div>
		<div class="span1"></div>
		
	</div>
<script type="text/javascript">
	$( document ).ready(function() {
		$("#delete").click(function(){
			var prod_select = $("#exampleFormControlSelect1").val();
			var r = confirm("¿Deseas eliminar definitivamente el producto "+prod_select+"?");
				if(r == true){
					$.getJSON( "php/borrar_prod.php",{prod:prod_select});
					alert("Su producto se ha eliminado correctamente");
				}else{
					location.reload();
				}
			
		});
	});
</script>	

	</body>
</html>