-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 16-04-2018 a las 11:20:01
-- Versión del servidor: 10.1.25-MariaDB
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `plp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cierre`
--

CREATE TABLE `cierre` (
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `importe` float NOT NULL,
  `nombre_pelu` varchar(75) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `precios`
--

CREATE TABLE `precios` (
  `id` int(3) NOT NULL,
  `nombre` varchar(75) NOT NULL,
  `pvp` float NOT NULL,
  `color` varchar(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `precios`
--

INSERT INTO `precios` (`id`, `nombre`, `pvp`, `color`) VALUES
(1, 'Producto', 2, '#5f68e7'),
(2, 'Corte Niña', 12, '#5f68e7'),
(3, 'Corte Niñas', 16, '#5f68e7'),
(4, 'Corte', 15, '#5f68e7'),
(5, 'Corte Señora', 15, '#5f68e7'),
(6, 'Corte Hombre', 11, '#0ccafa'),
(7, 'Corte Niño', 9, '#0ccafa'),
(8, 'Bebe', 5, '#0ccafa'),
(9, 'Permanente', 36, '#0ccafa'),
(10, 'Suplemento Color', 10, '#0ccafa'),
(11, 'Tinte Raiz', 13, '#3ef90c'),
(12, 'Tinte Pelo Corto', 14, '#3ef90c'),
(13, 'Tinte Pelo Medio', 17, '#3ef90c'),
(14, 'Tinte Pelo Largo', 21, '#3ef90c'),
(15, 'Baño Pelo Corto', 8, '#3ef90c'),
(16, 'Baño Pelo Medio', 12, '#ff9704'),
(17, 'Baño Pelo Largo', 18, '#ff9704'),
(18, 'Mechas Raiz', 28, '#ff9704'),
(19, 'Mechas Pelo Corto', 10, '#ff9704'),
(20, 'Mechas Pelo Medio', 16, '#ff9704'),
(21, 'Mechas Pelo Largo', 21, '#cf05d5'),
(22, 'Mechas Todo', 32, '#cf05d5'),
(23, 'Recogido', 50, '#cf05d5'),
(24, 'Semirecogido', 20, '#cf05d5'),
(25, 'Difusor', 8, '#cf05d5'),
(26, 'Plancha', 3, '#c8d804'),
(27, 'flequillo', 3, '#c8d804'),
(28, 'Secado Pelo Corto', 9, '#c8d804'),
(29, 'Secado Pelo Medio', 11, '#07c669'),
(30, 'Secado Pelo Largo', 13.5, '#07c669'),
(31, 'Descuento 5%', 0.95, '#ff0000'),
(32, 'Descuento 10%', 0.9, '#ff0000');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tickets`
--

CREATE TABLE `tickets` (
  `fecha` varchar(11) NOT NULL,
  `nombre` varchar(75) NOT NULL,
  `pvp` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tickets_copia`
--

CREATE TABLE `tickets_copia` (
  `fecha` varchar(11) NOT NULL,
  `nombre` varchar(75) NOT NULL,
  `pvp` float NOT NULL,
  `nombre_pelu` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tickets_copia_cierre`
--

CREATE TABLE `tickets_copia_cierre` (
  `fecha` varchar(11) NOT NULL,
  `hora` varchar(50) NOT NULL,
  `nombre` varchar(75) NOT NULL,
  `pvp` float NOT NULL,
  `nombre_pelu` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `precios`
--
ALTER TABLE `precios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `precios`
--
ALTER TABLE `precios`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
